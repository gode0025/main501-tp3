using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class mouvement : MonoBehaviour
{

    private bool showBox = false;
    private List<Camera> cameras;
    private int currentCameraIndex;

    // Start is called before the first frame update
    //Changer de caméra avec les touches P et O
    void Start()
    {
        cameras = new List<Camera>(FindObjectsOfType<Camera>());
        currentCameraIndex = 0;
        for(int i=0;i<cameras.Count;i++)
        {
            if(i == currentCameraIndex)
            {
                cameras[i].enabled = true;
            }
            else
            {
                cameras[i].enabled = false;
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.S)){
            transform.Translate(Vector3.down * 0.0030f);
        }
        if(Input.GetKey(KeyCode.Z)){
            transform.Translate(Vector3.up * 0.0030f);
            if(Input.GetKey(KeyCode.LeftAlt)){
                transform.Translate(Vector3.up * 0.004f);
            }
        }
        if(Input.GetKey(KeyCode.Q)){
            transform.Rotate(Vector3.forward * -0.15f);
        }
        if(Input.GetKey(KeyCode.D)){
            transform.Rotate(Vector3.forward * 0.15f);
        }
        //Commande pour changer de caméra
        if (Input.GetKeyDown(KeyCode.P))
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex++;
            if (currentCameraIndex == cameras.Count)
            {
                currentCameraIndex = 0;
            }
            cameras[currentCameraIndex].enabled = true;
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            cameras[currentCameraIndex].enabled = false;
            currentCameraIndex--;
            if (currentCameraIndex == -1)
            {
                currentCameraIndex = cameras.Count-1;
            }
            cameras[currentCameraIndex].enabled = true;
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            showBox = !showBox; //Affiche les commandes de la grue
        }
    }

    void OnGUI()
    {
        if(showBox){
            // Définir la position et la taille du rectangle
            Rect rect = new Rect(10, 10, 210, 220);
            
            // Dessiner le rectangle
            GUI.Box(rect, "Contrôles de la grue");

            // Ajouter les labels de texte
            GUI.Label(new Rect(20, 40, 180, 20), "Bouger : ZQSD");
            GUI.Label(new Rect(20, 60, 180, 20), "Chariot : Up et Down Arrow");
            GUI.Label(new Rect(20, 80, 180, 20), "Lever/Baisser Flèche : W/X");
            GUI.Label(new Rect(20, 100, 200, 20), "Tourner Flèche : Left/Right Arrow");
            GUI.Label(new Rect(20, 120, 180, 20), "Grappin : Left Shift/Ctrl");
            GUI.Label(new Rect(20, 140, 180, 20), "Déployer/replier pieds : A/E");
            GUI.Label(new Rect(20, 160, 180, 20), "Poser pieds : R/F");
            GUI.Label(new Rect(20, 180, 180, 20), "Changer de caméra : O/P");
            GUI.Label(new Rect(20, 200, 180, 20), "Lâcher objet : Espace");
        }
        if(!showBox){
            GUI.Box(new Rect(10, 10, 210, 25), "Commandes appuyer sur Tab");
        }
    }
}
