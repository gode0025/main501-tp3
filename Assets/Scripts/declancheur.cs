using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class declancheur : MonoBehaviour
{

    int stocks;
    // Start is called before the first frame update
    void Start()
    {
        stocks = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        // Vérifier si l'objet entrant est un cube
        if (other.gameObject.CompareTag("Cube"))
        {
            stocks++;
        }

    }

    private void OnTriggerExit(Collider other)
    {
        // Vérifier si l'objet sortant est un cube
        if (other.gameObject.CompareTag("Cube"))
        {
            stocks--;
        }
    }

    void OnGUI(){
        //Indique le nombre de cubes stockés en temps réels
        Rect rect = new Rect(Screen.width - 110, 10, 100, 20);
        GUI.Box(rect, "Stocks : " + stocks);
    }
}
